# Install
**Docker**

Install Docker and Docker-Compose

    git clone https://gitlab.com/d3fus/mc-twitch-bot.git
    cd mc-twitch-bot
    cd data
    cp config.json.dist config.json

Now edit the config.json with an text editor your choice. I will pick nvim.

    nvim config.json

**Edit this lines:**

the token you can find [here](https://twitchapps.com/tmi/)

    "tmi_token": "oauth:......"
    "channel": "",
    "bot_nick": "",
    "bot_prefix": "!",
  
 Minecraft rcon:
     

    "mc_server_ip": "127.0.0.1",
    "mc_server_rcon_pw": "",
    "mc_server_rcon_port": 28016,

Enable or disable modules:

    "module":{
     "streamlabs": true,
     "watchtime": true
    },

If Streamlabs is enabled, you have to fill your api socket token. You can get it [here](https://streamlabs.com/dashboard#/settings/api-settings)

    "streamlabs": {
     "api_token": "",
     ....

Now start the docker.

    cd ..
    docker-compose up


