import urllib, json, time
import db, config

def reward_user():
    while True:
        streamer = config.config['twitch_bot']['channel'] 
        url = 'http://tmi.twitch.tv/group/user/{}/chatters'.format(streamer)
        data = {}
        with urllib.request.urlopen(url) as url:
            data = json.loads(url.read())
        for group in data['chatters']:
            for user in data['chatters'][group]:
                if user != streamer and user != "moobot":
                    if not db.exist_user(user):
                        db.create_user(user)
                    old_token = db.get_user_token(user)
                    add_token = config.config['watchtime']['tokens']
                    db.write_user_token(user, old_token + add_token)
        time.sleep(60)
